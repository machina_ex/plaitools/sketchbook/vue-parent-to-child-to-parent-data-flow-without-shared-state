# basics

more boilerplate code, but maybe more orderly (to be discussed).

App.vue listens to "updateData" event emitted by each component.  It updates its parentData with the object send by using js Object.assign method.
The event is emitted with the vue contexts .emit method and includes a diff object of the parentData object which serves as single source of truth.

In total: Maybe better but not really clearer/easier to handle.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
